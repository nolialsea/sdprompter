import axios from 'axios'
import './App.css';
import {Buffer} from 'buffer'
import {useEffect, useRef, useState} from "react";
import {v4} from 'uuid'
import DrawApp from "./DrawApp";

function downloadBase64File(data) {
    if (!data) return
    const a = document.createElement("a")
    a.href = "data:image/png;base64," + data.image
    a.download = `${data.id}.png`
    a.click()
}


function downloadTextFile(text, filename = "data") {
    if (!text) return
    const a = document.createElement("a")
    a.href = 'data:text/plain;charset=utf-8,' + encodeURIComponent(text)
    a.download = `${filename}.json`
    a.click()
}

function downloadTextData(data) {
    downloadTextFile(JSON.stringify(data.config, null, 4), data.id)
}

const initialPrompts = [
    "beautiful cyberpunk Emma Watson in military uniform. attractive face. Portrait by Ansel Adams and Peter Kemp and Nina Leen and Edward Steichen, Photography. Cinematic, hyper realism, realistic proportions, dramatic lighting, high detail 4k",
    "beautiful cyberpunk Emma Watson in military uniform. attractive face. Portrait by john william waterhouse and Edwin Longsden Long and Theodore Ralli and Nasreddine Dinet, oil on canvas. Cinematic, hyper realism, realistic proportions, dramatic lighting, high detail 4k"
]

function fallbackCopyTextToClipboard(text) {
    const textArea = document.createElement("textarea");
    textArea.value = text;

    // Avoid scrolling to bottom
    textArea.style.top = "0";
    textArea.style.left = "0";
    textArea.style.position = "fixed";

    document.body.appendChild(textArea);
    textArea.focus();
    textArea.select();

    try {
        const successful = document.execCommand('copy');
        const msg = successful ? 'successful' : 'unsuccessful';
        console.log('Fallback: Copying text command was ' + msg);
    } catch (err) {
        console.error('Fallback: Oops, unable to copy', err);
    }

    document.body.removeChild(textArea);
}

function copyTextToClipboard(text) {
    if (!navigator.clipboard) {
        fallbackCopyTextToClipboard(text);
        return;
    }
    navigator.clipboard.writeText(text).then(function () {
        console.log('Async: Copying to clipboard was successful!');
    }, function (err) {
        console.error('Async: Could not copy text: ', err);
    });
}

function App() {
    const useLocalStorage = (storageKey, fallbackState) => {
        const storedValue = localStorage.getItem(storageKey)
        let stateValue

        if (!storedValue) {
            stateValue = fallbackState
        } else if (typeof fallbackState === "object") {
            try {
                stateValue = JSON.parse(localStorage.getItem(storageKey))
            } catch {
                stateValue = fallbackState
            }
        } else if (typeof fallbackState === "boolean") {
            stateValue = storedValue === "true"
        } else if (typeof fallbackState === "number") {
            stateValue = parseFloat(storedValue)
        } else {
            stateValue = storedValue
        }

        if (!stateValue) stateValue = fallbackState

        const [value, setValue] = useState(stateValue);

        useEffect(() => {
            const v = (typeof fallbackState === "object") ?
                JSON.stringify(value)
                : value
            localStorage.setItem(storageKey, v)
        }, [value, storageKey, fallbackState]);

        return [value, setValue];
    }

    const useForceUpdate = () => {
        const [, setValue] = useState(0); // integer state
        return () => setValue(value => value + 1); // update state to force render
        // An function that increment 👆🏻 the previous state like here
        // is better than directly setting `value + 1`
    }

    const [savedImages, setSavedImages] = useState([])
    const [currentData, setCurrentData] = useState({})
    const [currentDataVariation, setCurrentDataVariation] = useState({})
    const [prompt, setPrompt] = useLocalStorage("prompt", initialPrompts[1])
    const [variationPrompt, setVariationPrompt] = useLocalStorage("variationPrompt", initialPrompts[0])
    const [isGenerating, setGenerating] = useState(false)
    const [isBatchGenerating, setIsBatchGenerating] = useState(false)
    const [scale, setScale] = useLocalStorage("scale", 7.5)
    const [scaleVariation, setScaleVariation] = useLocalStorage("scaleVariation", 7.5)
    const [strength, setStrength] = useLocalStorage("strength", 0.7)
    const [steps, setSteps] = useLocalStorage("steps", 35)
    const [stepsVariation, setStepsVariation] = useLocalStorage("stepsVariation", 35)
    const [seed, setSeed] = useLocalStorage("seed", -1)
    const [seedVariation, setSeedVariation] = useLocalStorage("seedVariation", -1)
    const [seedIsRandom, setSeedIsRandom] = useLocalStorage("seedIsRandom", true)
    const [seedIsRandomVariation, setSeedIsRandomVariation] = useLocalStorage("seedIsRandomVariation", true)
    const [timer, setTimer] = useLocalStorage("timer", 0)
    const [width, setWidth] = useLocalStorage("width", 512)
    const [height, setHeight] = useLocalStorage("height", 512)
    const [backendUrl, setBackendUrl] = useLocalStorage("backendUrl", '')
    const [batch, setBatch] = useLocalStorage("batch", [])
    const [autoSave, setAutoSave] = useLocalStorage("autoSave", false)
    const forceUpdate = useForceUpdate()
    const inputRef = useRef(null)

    const generateImageBuffer = async (prompt, steps = 35, scale = 7.5, seed = null, width = 512, height = 512) => {
        try {
            return Buffer
                .from(
                    (await axios.get(
                            backendUrl
                            + (backendUrl?.endsWith('/') ? '' : `/`)
                            + `?steps=${steps}`
                            + `&scale=${scale}`
                            + `&width=${width}`
                            + `&height=${height}`
                            + (seed !== null ? `&seed=${seed}` : '')
                            + `&prompt=${prompt}`,
                            {responseType: 'arraybuffer', timeout: 1000 * 60 * 5})
                    ).data, 'binary')
                .toString('base64')
        } catch (e) {
            console.error(e)
            return null
        }
    }

    const generateImageVariationBuffer = async (img, prompt, steps = 35, scale = 7.5, seed = null, strength = 0.75) => {
        try {
            return Buffer
                .from(
                    (await axios.post(
                            backendUrl
                            + (backendUrl?.endsWith('/') ? '' : `/`)
                            + `img2img`,
                            {steps, scale, strength, seed, prompt, img}, {
                                responseType: 'arraybuffer',
                                timeout: 1000 * 60 * 5
                            })
                    ).data, 'binary')
                .toString('base64')
        } catch (e) {
            console.error(e)
            return null
        }
    }

    const swapImage = () => {
        setCurrentData(currentDataVariation)
        setCurrentDataVariation({})
        forceUpdate()
    }

    const keepImage = (data) => {
        setSavedImages([data ? data : currentData].concat(savedImages))
    }

    const keepImageVariation = (data) => {
        setSavedImages([data ? data : currentDataVariation].concat(savedImages))
    }

    const discardImage = () => {
        setCurrentData(null)
    }

    const discardImageVariation = () => {
        setCurrentDataVariation(null)
    }

    const discardAllImages = () => {
        setSavedImages([])
    }

    const saveData = (data) => {
        downloadBase64File(data)
        downloadTextData(data)
    }

    const saveImageOnSystem = () => {
        saveData(currentData)
    }

    const getInput = (e) => {
        setPrompt(e.target.value)
    }

    const getVariationPrompt = (e) => {
        setVariationPrompt(e.target.value)
    }

    const updateSteps = (e) => {
        setSteps(parseInt(e.target.value))
    }

    const updateStepsVariation = (e) => {
        setStepsVariation(parseInt(e.target.value))
    }

    const updateWidth = (e) => {
        setWidth(parseInt(e.target.value))
    }

    const updateHeight = (e) => {
        setHeight(parseInt(e.target.value))
    }

    const updateScale = (e) => {
        if (!isNaN(parseFloat(e.target.value)))
            setScale(parseFloat(e.target.value))
    }

    const updateScaleVariation = (e) => {
        if (!isNaN(parseFloat(e.target.value)))
            setScaleVariation(parseFloat(e.target.value))
    }

    const updateStrength = (e) => {
        if (!isNaN(parseFloat(e.target.value)))
            setStrength(parseFloat(e.target.value))
    }


    const updateSeed = (e) => {
        setSeed(e.target.value)
    }

    const updateSeedVariation = (e) => {
        setSeedVariation(e.target.value)
    }

    const updateStyle = (e) => {
        setBackendUrl(e.target.value)
    }

    const deleteImage = (i) => {
        const imgs = [...savedImages]
        imgs.splice(i, 1)
        setSavedImages(imgs)
    }

    const updateSeedIsRandom = () => {
        setSeedIsRandom(!seedIsRandom)
    }

    const updateSeedIsRandomVariation = () => {
        setSeedIsRandomVariation(!seedIsRandomVariation)
    }

    const copyConfig = (config) => {
        copyTextToClipboard(JSON.stringify(config, null, 4))
    }

    const loadConfig = async () => {
        const text = await navigator.clipboard.readText()
        loadConfigJ(JSON.parse(text))
    }

    const loadConfigJ = (config) => {
        setSeed(config.seed)
        setHeight(config.height)
        setWidth(config.width)
        setPrompt(config.prompt)
        setSteps(config.steps)
        setScale(config.scale)
        setSeedIsRandom(false)
    }

    // eslint-disable-next-line react-hooks/exhaustive-deps
    const updateTimer = async () => {
        if (timer > 0) {
            setTimer(timer - 1)
        }

        if (isBatchGenerating && !isGenerating) {
            const batch_ = [...batch]
            const item = batch_.shift()
            if (item) {
                setGenerating(true)
                setTimer((steps / 2 + 1) * (width * height / (512 * 512)))
                item.image = await generateImageBuffer(item.config.prompt, item.config.steps, item.config.scale, item.config.seed, item.config.width, item.config.height)
                if (!item?.image) return
                if (autoSave) {
                    saveData(item)
                }
                setCurrentData(item)
                setTimer(0)
                setBatch(batch_)
                setSavedImages([item].concat(savedImages))
                setGenerating(false)
            } else {
                setIsBatchGenerating(false)
            }
        }
    }

    const emptyBatch = async () => {
        setBatch([])
    }

    const interruptBatch = () => {
        setIsBatchGenerating(false)
    }

    const exportBatch = () => {
        downloadTextFile(JSON.stringify(batch, null, 4))
    }

    const executeBatch = () => {
        setIsBatchGenerating(true)
    }

    const parsePrompt = (prompt) => {
        if (typeof prompt === "string") {
            const keywords = prompt.match(/{[^}]*}/gi)?.map?.(e => e
                .replace('{', '')
                .replace('}', ''))

            if (keywords) {
                const prompts = []
                const placeholderChoices = {}
                for (let placeholder of keywords) {
                    if (!placeholderChoices[placeholder]) placeholderChoices[placeholder] = []
                    const choices = placeholder.split('|')
                    for (let choice of choices) {
                        prompts.push(prompt.replace(`{${placeholder}}`, choice))
                    }
                }

                return parsePrompt(prompts)
            } else {
                return [prompt]
            }
        } else if (Array.isArray(prompt)) {
            return prompt.map(parsePrompt).flat()
        }
    }

    const addToBatch = async () => {
        const seed_ = seedIsRandom ? Math.floor(Math.random() * 1000000) : seed
        if (seedIsRandom) {
            setSeed(seed_)
        }
        const keywords = prompt.match(/{[^}]*}/gi)?.map?.(e => e
            .replace('{', '')
            .replace('}', ''))
        const newBatch = []

        if (keywords) {
            const prompts = [...new Set(parsePrompt(prompt))]

            for (let p of prompts) {
                const data = {
                    id: v4(),
                    image: null,
                    config: {prompt: p, steps, scale, seed: seed_, width, height}
                }
                newBatch.push(data)
            }
        } else {
            const data = {
                id: v4(),
                image: null,
                config: {prompt: prompt, steps, scale, seed: seed_, width, height}
            }
            newBatch.push(data)
        }

        setBatch(batch.concat(newBatch))
    }

    const getVariationImage = async () => {
        setGenerating(true)

        if (!currentData || !currentData.image) {
            setGenerating(false)
            return
        }

        const seed_ = seedIsRandomVariation ? Math.floor(Math.random() * 1000000) : seedVariation
        if (seedIsRandom) {
            setSeedVariation(seed_)
        }
        setTimer((steps / 2 + 1) * (width * height / (512 * 512)))
        updateTimer()
        const keywords = variationPrompt.match(/{([^}]*)}/gi)?.map?.(e => e
            .replace('{', '')
            .replace('}', ''))
        let prompt_ = variationPrompt
        if (keywords) {
            for (let placeholder of keywords) {
                const choices = placeholder.split('|')
                prompt_ = prompt_.replace(`{${placeholder}}`, choices[Math.floor(Math.random() * choices.length)])
            }
        }
        const data = {
            id: v4(),
            image: await generateImageVariationBuffer(currentData.image, prompt_, stepsVariation, scaleVariation, seed_, strength),
            config: {
                prompt: prompt_,
                steps: stepsVariation,
                scale: scaleVariation,
                seed: seed_,
                width,
                height,
                strength
            }
        }
        setCurrentDataVariation(data)
        setTimer(0)

        setGenerating(false)
    }

    const getImage = async () => {
        setGenerating(true)

        const seed_ = seedIsRandom ? Math.floor(Math.random() * 1000000) : seed
        if (seedIsRandom) {
            setSeed(seed_)
        }
        setTimer((steps / 2 + 1) * (width * height / (512 * 512)))
        updateTimer()
        const keywords = prompt.match(/{([^}]*)}/gi)?.map?.(e => e
            .replace('{', '')
            .replace('}', ''))
        let prompt_ = prompt
        if (keywords) {
            for (let placeholder of keywords) {
                const choices = placeholder.split('|')
                prompt_ = prompt_.replace(`{${placeholder}}`, choices[Math.floor(Math.random() * choices.length)])
            }
        }
        const data = {
            id: v4(),
            image: await generateImageBuffer(prompt_, steps, scale, seed_, width, height),
            config: {prompt: prompt_, steps, scale, seed: seed_, width, height}
        }
        setCurrentData(data)
        setTimer(0)

        setGenerating(false)
        forceUpdate()
    }

    useEffect(() => {
        const interval = setInterval(updateTimer, 1000)

        return () => clearInterval(interval);
    }, [updateTimer])


    const handleOndragOver = (event) => {
        event.preventDefault();
    }
    const handleOndrop = async (event) => {
        //prevent the browser from opening the image
        event.preventDefault();
        event.stopPropagation();
        //let's grab the image file
        let imageFile = event.dataTransfer.files[0];
        await handleFile(imageFile)
    }

    const handleFile = async (file) => {
        //you can carry out any file validations here...
        const d = currentData
        d.image = Buffer.from(await file.arrayBuffer())
            .toString('base64')
        setCurrentData(d)
        forceUpdate()
    }

    const importBatch = () => {
        // 👇️ open file input box on click of other element
        inputRef.current.click();
    };

    const handleFileChange = event => {
        const fileObj = event.target.files && event.target.files[0];
        if (!fileObj) {
            return;
        }

        // 👇️ reset file input
        event.target.value = null;

        const fr = new FileReader();
        fr.onload = function (result) {
            setBatch(JSON.parse(fr.result))
        }

        fr.readAsText(fileObj)
    }

    return (
        <div className="App">
            <DrawApp width={width} height={height} imgSrc={currentData?.image} onGetImage={(img) => {
                currentData.image = img.replace("data:image/png;base64,", '')
                setCurrentData(currentData)
                forceUpdate()
            }}/>
            <div className="drop_zone" onDragOver={handleOndragOver}
                 onDrop={handleOndrop}>
                {!!currentData?.image ?
                    <img src={`data:image/png;base64,${currentData?.image}`}
                         className="App-logo" alt="" onDoubleClick={() => downloadBase64File(currentData)}/>
                    : <img className="App-logo" alt=""/>
                }
                {!!currentDataVariation?.image ?
                    <img src={`data:image/png;base64,${currentDataVariation?.image}`}
                         className="App-logo" alt="" onDoubleClick={() => downloadBase64File(currentDataVariation)}/>
                    : <img className="App-logo" alt=""/>
                }
            </div>

            <br/>
            <label
                className="timer">{timer > 0 ? `ETA: ${timer.toFixed(0).padStart(2, '0')}` : 'ETA: 00'} seconds</label>

            <br/>
            <label>Backend URL: </label><input onChange={updateStyle} value={backendUrl}/>
            <textarea onChange={getInput} value={prompt}/>

            <label>Width: </label><input onChange={updateWidth} value={width}/>
            <label>Height: </label><input onChange={updateHeight} value={height}/>
            <label>Steps: </label><input onChange={updateSteps} value={steps}/>
            <label>Scale: </label>
            <input onChange={updateScale} value={scale}/>
            <input onChange={updateScale} type="range" min="0" max="20" step="0.1" value={scale}/>
            <button onClick={updateSeedIsRandom}>{seedIsRandom ? 'Seed is random' : 'Seed is not random'}</button>
            <label>Seed: </label><input onChange={updateSeed} value={seed} disabled={seedIsRandom}/>

            <br/>
            <button onClick={getImage} disabled={isGenerating}>Generate</button>
            <button onClick={() => {
                keepImage(currentData)
            }} disabled={!currentData || isGenerating}>Keep
            </button>
            <button onClick={discardImage} disabled={!currentData || isGenerating}>Discard</button>
            <button onClick={saveImageOnSystem} disabled={!currentData}>Save to disk</button>


            <br/>
            <button onClick={() => {
                copyConfig({prompt, seed, width, height, steps, scale})
            }}>Copy Config
            </button>
            <button onClick={() => {
                loadConfig().then(r => null)
            }}>Load Config
            </button>

            <hr/>
            <label>Variation Prompt</label>
            <textarea onChange={getVariationPrompt} value={variationPrompt}/>
            <label>Steps: </label><input onChange={updateStepsVariation} value={stepsVariation}/>
            <label>Strength: </label>
            <input onChange={updateStrength} value={strength}/>
            <input onChange={updateStrength} type="range" min="0" max="1" step="0.05" value={strength}/>
            <label>Scale: </label>
            <input onChange={updateScaleVariation} value={scaleVariation}/>
            <input onChange={updateScaleVariation} type="range" min="0" max="20" step="0.1" value={scaleVariation}/>
            <button
                onClick={updateSeedIsRandomVariation}>{seedIsRandomVariation ? 'Seed is random' : 'Seed is not random'}</button>
            <label>Seed: </label><input onChange={updateSeedVariation} value={seedVariation}
                                        disabled={seedIsRandomVariation}/>

            <br/>
            <button onClick={getVariationImage} disabled={!currentData || !currentData?.image || isGenerating}>Generate
                Variation
            </button>
            <button onClick={() => {
                keepImageVariation(currentDataVariation)
            }} disabled={!currentDataVariation || !currentDataVariation?.image || isGenerating}>Keep Variation
            </button>
            <button onClick={swapImage}
                    disabled={!currentDataVariation || !currentDataVariation?.image || isGenerating}>Swap Variation
            </button>
            <button onClick={discardImageVariation}
                    disabled={!currentDataVariation || !currentDataVariation?.image || isGenerating}>Discard Variation
            </button>

            <hr/>
            <br/>
            <label>Auto save: </label><input type="checkbox"
                                             onChange={() => {
                                                 setAutoSave(!autoSave)
                                             }} checked={autoSave}/>

            <br/>
            <button onClick={addToBatch} disabled={isGenerating}>Add to Batch</button>
            <button onClick={executeBatch} disabled={isGenerating || batch?.length === 0}>Generate Batch</button>
            <button onClick={emptyBatch} disabled={isGenerating || batch?.length === 0}>Empty Batch</button>
            <button onClick={interruptBatch} disabled={!isBatchGenerating}>Interrupt Batch</button>
            <button onClick={importBatch}>Import Batch</button>
            <button onClick={exportBatch}>Export Batch</button>
            <input
                style={{display: 'none'}}
                ref={inputRef}
                type="file"
                onChange={handleFileChange}
            />
            <label className="tooltip">
                <span className="tooltiptext">{
                    batch?.map?.((d) => {
                        return <li key={v4()}>{d.config.prompt}</li>
                    })
                }</span>
                Batch size: {batch?.length}
            </label>

            <br/>
            <hr/>
            <button onClick={discardAllImages} disabled={isGenerating}>Discard All</button>
            <br/>
            {savedImages?.map?.((data, i) => {
                if (!data) return <span>ERROR</span>
                const id = v4()
                return (
                    <span key={id} className="inline-block tooltip">
                        <span className="tooltiptext">{JSON.stringify(data.config, null, 4)}</span>
                        <img id={'' + id} src={`data:image/png;base64,${data?.image}`} className="App-logo" alt=""
                             onDoubleClick={() => downloadBase64File(currentDataVariation)}/>
                        <br/>
                        <button onClick={() => {
                            saveData(data)
                        }}>Save</button> | <button onClick={() => {
                        loadConfigJ(data.config)
                    }}>Use Config</button> | <button disabled={isGenerating}
                                                     onClick={() => {
                                                         deleteImage(i)
                                                     }}>Delete</button>
                    </span>
                )
            })}
        </div>
    );
}

export default App;
