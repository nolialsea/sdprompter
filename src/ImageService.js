import {Buffer} from "buffer";
import axios from "axios";
import {mkdirSync, writeFileSync} from "fs";
import {v4} from "uuid";
import joinImages from "join-images";
import sharp from 'sharp'

try {
    mkdirSync('output')
} catch {

}

export default class ImageService {
    constructor(backendUrl, savePath = null, autoSave = true) {
        this.backendUrl = backendUrl
        this.autoSave = false
        this.savePath = savePath
        this.autoSave = autoSave
    }

    /**
     *
     * @param {Buffer} imgBuffer
     * @param {number} width
     * @param {number} height
     * @param {string|null} filename
     * @param {string|null} savePath
     */
    async splitGrid(imgBuffer, width, height, filename = null, savePath = null) {
        const image = sharp(imgBuffer)
        const id = v4()

        const metadata = await image.metadata()
        for (let x = 0; x < metadata.width; x += width) {
            for (let y = 0; y < metadata.height; y += height) {
                const filename_ = filename ? `/${filename}_${x / width}_${y / height}.png` : `/${id}_${x / width}_${y / height}.png`
                const savePath_ = savePath || (this.savePath || `output`)
                const extractedBuffer = await image
                    .extract({left: x, top: y, width, height})
                    .toBuffer()
                writeFileSync(savePath_ + `/${filename_}`, extractedBuffer)
            }
        }
    }

    async bip() {
        console.log('\u0007')
    }

    /**
     *
     * @param {Buffer[]} imageBuffers
     * @param {"horizontal"|"vertical"} direction
     * @param {boolean} save
     * @return {Promise<null|Buffer>}
     */
    async join(imageBuffers, direction, save = false) {
        const buff = await (
            await joinImages(
                imageBuffers.map(ib => {
                    return {src: ib, offsetX: 0, offsetY: 0}
                }),
                {direction})
        ).png().toBuffer()
        if (save && buff) {
            writeFileSync((this.savePath || `output`) + `/${v4()}.png`, buff)
        }
        return buff
    }

    /**
     *
     * @param {Buffer[]} imageBuffers
     * @param {boolean} save
     * @return {Promise<null|Buffer>}
     */
    async joinRight(imageBuffers, save = false) {
        return await this.join(imageBuffers, 'horizontal', save)
    }

    /**
     *
     * @param {Buffer[]} imageBuffers
     * @param {boolean} save
     * @return {Promise<null|Buffer>}
     */
    async joinDown(imageBuffers, save = false) {
        return await this.join(imageBuffers, 'vertical', save)
    }

    /**
     *
     * @param {Buffer} imgBuffer
     * @param {Buffer} maskBuffer
     * @param {string} prompt
     * @param {Object<{steps: number?, scale: number?, seed: number?, strength: number?}>} opts
     * @param {boolean} autoSave
     * @return {Promise<null|Buffer>}
     */
    async generateInpainting(imgBuffer, maskBuffer, prompt, opts = {
        steps: 35,
        width: 512,
        height: 512,
        scale: 7.5,
        seed: null,
        strength: 0.7
    }, autoSave = false) {
        try {
            const obj = {
                seed: opts.seed || Math.floor(Math.random() * 1000000),
                steps: opts.steps || 35,
                scale: opts.scale || 7.5,
                width: opts.width || 512,
                height: opts.height || 512,
                strength: opts.strength || 0.7,
                prompt
            }
            const uuid = v4()
            const buff = Buffer
                .from(
                    (await axios.post(
                            this.backendUrl
                            + (this.backendUrl.endsWith('/') ? '' : `/`)
                            + `inpainting`,
                            {
                                steps: obj.steps,
                                scale: obj.scale,
                                strength: obj.strength,
                                seed: obj.seed,
                                prompt,
                                width: obj.width,
                                height: obj.height,
                                img: imgBuffer.toString('base64'),
                                mask: maskBuffer.toString('base64')
                            }, {responseType: 'arraybuffer', timeout: 1000 * 60 * 5})
                    ).data, 'binary')
            if (this.autoSave || autoSave) {
                const fileName = `${uuid}_${obj.seed}_${obj.steps}_${obj.scale.toString().replace('.', '-')}`
                writeFileSync((this.savePath || `output`) + `/${fileName}.png`, buff)
            }
            return buff
        } catch (e) {
            console.error(e)
        }
    }

    /**
     *
     * @param {Buffer} imgBuffer
     * @param {string} prompt
     * @param {Object<{steps: number?, scale: number?, seed: number?, strength: number?}>} opts
     * @param {boolean} autoSave
     * @return {Promise<null|Buffer>}
     */
    async generateImageVariation(imgBuffer, prompt, opts = {
        steps: 35,
        width: 512,
        height: 512,
        scale: 7.5,
        seed: null,
        strength: 0.7
    }, autoSave = false) {
        try {
            const obj = {
                seed: opts.seed || Math.floor(Math.random() * 1000000),
                width: opts.width || 512,
                height: opts.height || 512,
                steps: opts.steps || 35,
                scale: opts.scale || 7.5,
                strength: opts.strength || 0.7,
                prompt
            }
            const uuid = v4()
            const buff = Buffer
                .from(
                    (await axios.post(
                            this.backendUrl
                            + (this.backendUrl.endsWith('/') ? '' : `/`)
                            + `img2img`,
                            {
                                steps: obj.steps,
                                scale: obj.scale,
                                strength: obj.strength,
                                seed: obj.seed,
                                prompt,
                                width: obj.width,
                                height: obj.height,
                                img: imgBuffer.toString('base64')
                            }, {responseType: 'arraybuffer', timeout: 1000 * 60 * 5})
                    ).data, 'binary')
            if (this.autoSave || autoSave) {
                const fileName = `${uuid}_${obj.seed}_${obj.steps}_${obj.scale.toString().replace('.', '-')}`
                writeFileSync((this.savePath || `output`) + `/${fileName}.png`, buff)
            }
            return buff
        } catch (e) {
            console.error(e)
        }
    }

    /**
     *
     * @param {string} prompt
     * @param {Object<{steps: number?, scale: number?, seed: number?, width: number?, height: number?}>} opts
     * @param {boolean} autoSave
     * @return {Promise<null|Buffer>}
     */
    async generateImage(prompt, opts = {steps: 35, scale: 7.5, seed: null, width: 512, height: 512}, autoSave = false) {
        try {
            const uuid = v4()
            const obj = {
                seed: opts.seed || Math.floor(Math.random() * 1000000),
                width: opts.width || 512,
                height: opts.height || 512,
                steps: opts.steps || 35,
                scale: opts.scale || 7.5,
                prompt
            }
            const buff = Buffer
                .from(
                    (await axios.post(
                            this.backendUrl,
                            {
                                seed: opts.seed || Math.floor(Math.random() * 1000000),
                                width: opts.width || 512,
                                height: opts.height || 512,
                                steps: opts.steps || 35,
                                scale: opts.scale || 7.5,
                                prompt
                            },
                            {responseType: 'arraybuffer', timeout: 1000 * 60 * 5})
                    ).data, 'binary')
            if (this.autoSave || autoSave) {
                const fileName = `${uuid}_${obj.seed}_${obj.steps}_${obj.scale.toString().replace('.', '-')}`
                writeFileSync((this.savePath || `output`) + `/${fileName}.png`, buff)
            }
            return buff
        } catch (e) {
            console.error("Error!", e)
        }
    }
}
