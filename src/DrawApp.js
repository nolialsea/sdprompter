import React, {useRef, useState} from 'react';
import CanvasDraw from "react-canvas-draw";
import {Buffer} from "buffer";
import {SketchPicker} from 'react-color'
import {Collapse} from 'react-collapse';

function DrawApp(props) {
    const canvasRef = useRef(null);

    const [base64Image, setBase64Image] = useState("")
    const [brushSize, setBrushSize] = useState(16)
    const [brushColor, setBrushColor] = useState("#888")
    const [isOpened, setIsOpened] = useState(false)

    if (canvasRef.current) {
        // this will get the canvas HTML element where everyhting that's painted is drawn
        // and call the toDataURL() function on it
        // console.log(canvasRef.current.canvasContainer.children[1].toDataURL());
    }

    const handleOndragOver = (event) => {
        event.preventDefault();
    }
    const handleOndrop = async (event) => {
        event.preventDefault();
        event.stopPropagation();
        let imageFile = event.dataTransfer.files[0];
        await handleFile(imageFile)
    }

    const handleFile = async (file) => {
        setBase64Image(Buffer.from(await file?.arrayBuffer?.())
            .toString('base64'))
    }

    const getImage = (callback) => {
        const ctxBackground = canvasRef.current.canvasContainer.children[0].getContext("2d")
        ctxBackground.drawImage(
            canvasRef.current.canvasContainer.children[1],
            0,
            0,
            props.width,
            props.height)
        callback(canvasRef.current.canvasContainer.children[0].toDataURL())
    }

    const updateBrushSize = async (e) => {
        setBrushSize(parseInt(e.target.value))
    }

    const resetDrawing = async () => {
        const ctx = canvasRef.current.canvasContainer.children[1].getContext("2d")
        ctx.clearRect(0, 0, 2048, 2048)
    }

    return (
        <>
            <button onClick={() => {
                setIsOpened(!isOpened)
            }}>Open/Close Paint Editor
            </button>
            <div hidden={isOpened ? false : 'hidden'}>
                <Collapse isOpened={isOpened}>
                    <CanvasDraw
                        className="drop_zone"
                        onDragOver={handleOndragOver}
                        onDrop={handleOndrop}
                        ref={canvasRef}
                        lazyRadius={0}

                        brushColor={brushColor}
                        brushRadius={brushSize}
                        canvasWidth={props.width || 512}
                        canvasHeight={props.height || 512}
                        imgSrc={`data:image/png;base64,${props.imgSrc ? props.imgSrc : base64Image}`}
                    />
                    <label>Brush size: </label>
                    <input onChange={updateBrushSize} value={brushSize}/>
                    <input onChange={updateBrushSize} type="range" min="1" max="128" step="1" value={brushSize}/>
                    <button onClick={() => getImage(props.onGetImage)}>Get Image</button>
                    <button onClick={resetDrawing}>Reset</button>
                    <SketchPicker color={brushColor} onChangeComplete={(color) => {
                        setBrushColor(color.hex)
                    }}/>
                </Collapse>
            </div>
        </>
    );
}

export default DrawApp