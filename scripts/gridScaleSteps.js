import ImageService from "../src/ImageService.js"
import {mkdirSync, writeFileSync} from "fs";
import {v4} from "uuid";


try {
    mkdirSync(savePath)
} catch {

}

const args = process.argv.slice(2)
if (args.length < 9) {
    console.log(`Bad script usage! Try like this:`)
    console.log(`node ./scripts/runGridScaleSteps.js "http://backend_url/" "C:/path/to/output/dir" "your prompt here" scaleStart scaleEnd scaleIncrement stepsStart stepsEnd stepsIncrement seed=42 width=512 height=512 saveAll=false`)
    process.exit(1)
}
const backendUrl = args.shift() // "http://localhost:5000/"
const savePath = args.shift() // "C:\\Users\\Noli\\Desktop\\tmp"

const prompt = args.shift()
const scaleStart = parseFloat(args.shift())
const scaleEnd = parseFloat(args.shift())
const scaleIncrement = parseFloat(args.shift())
const stepsStart = parseInt(args.shift())
const stepsEnd = parseInt(args.shift())
const stepsIncrement = parseInt(args.shift())

if (isNaN(scaleStart) || isNaN(scaleEnd) || isNaN(scaleIncrement)) {
    console.log(`Bad script usage! Scale values need to be floating numbers!`)
    process.exit(1)
}
if (isNaN(stepsStart) || isNaN(stepsEnd) || isNaN(stepsIncrement)) {
    console.log(`Bad script usage! Steps values need to be integral numbers!`)
    process.exit(1)
}

let seed = getRandomSeed()
let width = 512
let height = 512
let saveAll = false


if (args.length > 0) {
    const s = args.find(a => a.startsWith("seed="))
    if (s) {
        seed = parseInt(s.replace('seed=', ''))
    }

    const w = args.find(a => a.startsWith("width="))
    if (w) {
        width = parseInt(w.replace('width=', ''))
    }

    const h = args.find(a => a.startsWith("height="))
    if (h) {
        height = parseInt(h.replace('height=', ''))
    }

    const sa = args.find(a => a.toLowerCase().startsWith("saveall="))
    if (sa) {
        saveAll = h.toLowerCase().replace('saveall=', '') === "true"
    }
}

const imgService = new ImageService(backendUrl, savePath, saveAll)


const obj = {
    prompt,
    scaleStart,
    scaleEnd,
    scaleIncrement,
    stepsStart,
    stepsEnd,
    stepsIncrement,
    seed,
    width,
    height
}

console.log(
    JSON.stringify(obj, null, 4)
)

function getRandomSeed() {
    return Math.floor(Math.random() * 1000000)
}

async function main() {
    const lineBuffers = []
    for (let scale = scaleStart; scale <= scaleEnd; scale += scaleIncrement) {
        const imageBuffers = []
        for (let steps = stepsStart; steps <= stepsEnd; steps += stepsIncrement) {
            imageBuffers.push(await imgService.generateImage(prompt, {
                seed,
                scale,
                steps,
                width,
                height
            }))
            console.log(`Image { scale: ${scale}, steps: ${steps}} created!`)
        }
        lineBuffers.push(await ImageService.joinRight(imageBuffers))
        console.log("Line created!")
    }
    const lineBuffer = await ImageService.joinDown(lineBuffers)
    if (lineBuffer instanceof Buffer) {
        const id = v4()
        const filepath = (savePath || `output`) + `/${seed}_${id}.png`
        console.log(`Grid saved in ${filepath}`)
        writeFileSync(filepath, lineBuffer)
    } else {
        console.log("An error happened... No buffer to save")
    }
}

main().then(function () {
    console.log("Done!")
})
