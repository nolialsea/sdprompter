import ImageService from "../src/ImageService.js"
import {mkdirSync} from "fs";
import sharp from "sharp";

const args = process.argv.slice(2)

const img = args.shift()
const width = parseInt(args.shift())
const height = parseInt(args.shift())
const folder = args.shift() || "split"

console.log({
    width,
    height,
    folder
})

const imgService = new ImageService("backendUrl", folder)
try {
    mkdirSync(folder)
} catch {

}

async function main() {

    await imgService.splitGrid(await sharp(img).toBuffer(), width, height)

}

main().then()