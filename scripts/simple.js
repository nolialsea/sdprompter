import ImageService from "../src/ImageService.js"


const backendUrl = "http://localhost:5000/"
const savePath = "C:\\Users\\Noli\\Desktop\\tmp"
const saveAll = false

const imgService = new ImageService(backendUrl, savePath, saveAll)

function getRandomSeed() {
    return Math.floor(Math.random() * 100000000)
}

async function main() {
    const prompt = "portrait photography of scarlett johansson"
    const width = 576
    const height = 768
    const scale = 7.5
    const steps = 35

    while (true) {
        await imgService.generateImage(prompt, {
            seed: getRandomSeed(),
            scale,
            steps,
            width,
            height
        }, true)
    }
}

main().then(function () {
    console.log("Done!")
})
