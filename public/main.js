const canvas = document.getElementById('canvas');
const canvasHidden = document.getElementById('canvas_hidden');
const canvasSelection = document.getElementById('canvas_selection');
const canvasMask = document.getElementById('canvas_mask');
const canvasResult = document.getElementById('canvas_result');
const canvasResultComplete = document.getElementById('canvas_result_complete');
const canvasResultSelection = document.getElementById('canvas_result_selection');
const ctx = canvas.getContext('2d');
const ctxHidden = canvasHidden.getContext('2d');
const ctxSelection = canvasSelection.getContext('2d');
const ctxMask = canvasMask.getContext('2d');
const ctxResult = canvasResult.getContext('2d');
const ctxResultComplete = canvasResultComplete.getContext('2d');
const ctxResultSelection = canvasResultSelection.getContext('2d');
const mouse = {x: 352, y: 256, isDown: false}
const mouseMask = {x: 352, y: 256, isDown: false}
const inputWidth = document.getElementById('input_width')
const inputHeight = document.getElementById('input_height')
const inputZoom = document.getElementById('input_zoom')
let imageOnCanvas
let imageResult
let lastGenWasInpaint = false
let maskMode = 'Masking'

/**
 *
 * @param {string} prompt
 * @param {Object<{steps: number?, scale: number?, seed: number?, width: number?, height: number?}>} opts
 * @param {boolean} autoSave
 * @return {Promise<null|Buffer>}
 */
async function generateImage(prompt, opts = {
    steps: 35,
    scale: 7.5,
    seed: null,
    width: 512,
    height: 512
}, autoSave = false) {
    try {
        const obj = {
            seed: opts.seed || Math.floor(Math.random() * 1000000),
            width: opts.width || 512,
            height: opts.height || 512,
            steps: opts.steps || 35,
            scale: opts.scale || 7.5,
            prompt
        }
        const buff = Buffer
            .from(
                (await axios.get(
                        document.getElementById('input_backend').value
                        + `?steps=${obj.steps}`
                        + `&scale=${obj.scale}`
                        + `&width=${obj.width}`
                        + `&height=${obj.height}`
                        + (obj.seed !== null ? `&seed=${obj.seed}` : '')
                        + `&prompt=${prompt}`,
                        {responseType: 'arraybuffer', timeout: 1000 * 60 * 60})
                ).data, 'binary')
        return buff
    } catch (e) {
        console.error("Error!", e)
    }
}

/**
 *
 * @param {string} imgB64
 * @param {string} maskB64
 * @param {string} prompt
 * @param {Object<{steps: number?, scale: number?, seed: number?, strength: number?}>} opts
 * @param {boolean} autoSave
 * @return {Promise<null|Buffer>}
 */
async function generateInpainting(imgB64, maskB64, prompt, opts = {
    steps: 35,
    width: 512,
    height: 512,
    scale: 7.5,
    seed: null,
    strength: 0.7
}, autoSave = false) {
    try {
        const obj = {
            seed: opts.seed || Math.floor(Math.random() * 1000000),
            steps: opts.steps || 35,
            scale: opts.scale || 7.5,
            width: opts.width || 512,
            height: opts.height || 512,
            strength: opts.strength || 0.7,
            prompt
        }
        const buff = Buffer
            .from(
                (await axios.post(
                        document.getElementById('input_backend').value
                        + (document.getElementById('input_backend').value.endsWith('/') ? '' : `/`)
                        + `inpainting`,
                        {
                            prompt,
                            steps: obj.steps,
                            scale: obj.scale,
                            strength: obj.strength,
                            seed: obj.seed,
                            width: obj.width,
                            height: obj.height,
                            img: imgB64,
                            mask: maskB64
                        }, {responseType: 'arraybuffer', timeout: 1000 * 60 * 60})
                ).data, 'binary')
        return buff
    } catch (e) {
        console.error(e)
    }
}

/**
 *
 * @param {string} imgB64
 * @param {string} prompt
 * @param {Object<{steps: number?, scale: number?, seed: number?, strength: number?}>} opts
 * @param {boolean} autoSave
 * @return {Promise<null|Buffer>}
 */
async function generateImageVariation(imgB64, prompt, opts = {
    steps: 35,
    width: 512,
    height: 512,
    scale: 7.5,
    seed: null,
    strength: 0.7
}, autoSave = false) {
    try {
        const obj = {
            seed: opts.seed || Math.floor(Math.random() * 1000000),
            width: opts.width || 512,
            height: opts.height || 512,
            steps: opts.steps || 35,
            scale: opts.scale || 7.5,
            strength: opts.strength || 0.7,
            prompt
        }
        return Buffer
            .from(
                (await axios.post(
                        document.getElementById('input_backend').value
                        + (document.getElementById('input_backend').value.endsWith('/') ? '' : `/`)
                        + `inpainting`,
                        {
                            prompt,
                            steps: obj.steps,
                            scale: obj.scale,
                            strength: obj.strength,
                            seed: obj.seed,
                            width: obj.width,
                            height: obj.height,
                            img: imgB64
                        }, {responseType: 'arraybuffer', timeout: 1000 * 60 * 60})
                ).data, 'binary')
    } catch (e) {
        console.error(e)
    }
}

function dragOverHandler(ev) {
    ev.preventDefault();
}

const divDropzone = document.getElementById('div_dropzone')
divDropzone.ondragover = divDropzone.ondragenter = function (evt) {
    evt.preventDefault();
}
divDropzone.ondrop = onImage

document.querySelector('#input_file').addEventListener('change', (e) => {
    e.preventDefault();
    e.stopPropagation();
    const fileList = e.target.files;
    loadImage(fileList[0])
})

function loadImage(image) {
    const reader = new FileReader();
    reader.readAsDataURL(image);
    reader.onload = () => {
        const image = new Image();
        image.onload = () => {
            const canvas = document.querySelector('canvas');
            canvas.width = image.width;
            canvas.height = image.height;
            canvasHidden.width = image.width;
            canvasHidden.height = image.height;
            const context = canvas.getContext('2d');
            context.drawImage(image, 0, 0);
            ctxHidden.drawImage(image, 0, 0)
            imageOnCanvas = image
        }
        image.src = reader.result;
    }
}

function onImage(e) {
    e.preventDefault();
    e.stopPropagation();
    loadImage(e.dataTransfer.files[0])
}

function toggleButtons() {
    const btnGenInpaint = document.querySelector('#btn_generate_inpainting')
    const btnGenPaint = document.querySelector('#btn_generate_painting')
    const btnRepaint = document.querySelector('#btn_generate_repainting')

    btnGenInpaint.disabled = !btnGenInpaint.disabled
    btnGenPaint.disabled = !btnGenPaint.disabled
    btnRepaint.disabled = !btnRepaint.disabled
}


document.querySelector('#btn_download_full_image').addEventListener('click', function () {
    const dt = canvasHidden.toDataURL('image/png')
    this.href = dt.replace(/^data:image\/[^;]/, 'data:application/octet-stream')
}, false)

document.querySelector('#btn_result_toggle').addEventListener('click', async () => {
    document.querySelector('#canvas_result_complete').hidden = !document.querySelector('#canvas_result_complete').hidden
})

document.querySelector('#btn_full_image_toggle').addEventListener('click', async () => {
    document.querySelector('#canvas_hidden').hidden = !document.querySelector('#canvas_hidden').hidden
})

document.querySelector('input').addEventListener('drop', onImage);

document.querySelector('#btn_center_viewport').addEventListener('click', () => {
    mouse.x = canvas.width / 2
    mouse.y = canvas.height / 2
})

document.querySelector('#canvas').addEventListener('mousemove', (e) => {
    if (mouse.isDown) {
        mouse.x = e.clientX - e.target.offsetLeft + document.body.scrollLeft
            + document.documentElement.scrollLeft
        mouse.y = e.clientY - e.target.offsetTop + document.body.scrollTop
            + document.documentElement.scrollTop
    }
});
document.querySelector('#canvas').addEventListener('mousedown', (e) => {
    mouse.isDown = true
    mouse.x = e.clientX - e.target.offsetLeft + document.body.scrollLeft
        + document.documentElement.scrollLeft
    mouse.y = e.clientY - e.target.offsetTop + document.body.scrollTop
        + document.documentElement.scrollTop
});
document.querySelector('#canvas').addEventListener('mouseup', (e) => {
    mouse.isDown = false
    mouse.x = e.clientX - e.target.offsetLeft + document.body.scrollLeft
        + document.documentElement.scrollLeft
    mouse.y = e.clientY - e.target.offsetTop + document.body.scrollTop
        + document.documentElement.scrollTop
});

document.querySelector('#btn_mask_clear').addEventListener('click', (e) => {
    ctxMask.beginPath()
    ctxMask.clearRect(0, 0, inputWidth.value, inputHeight.value)
});
document.querySelector('#canvas_mask').addEventListener('mousemove', (e) => {
    const rect = canvasMask.getBoundingClientRect()
    mouseMask.x = e.clientX - rect.left
    mouseMask.y = e.clientY - rect.top
});
document.querySelector('#canvas_mask').addEventListener('mousedown', (e) => {
    mouseMask.isDown = true
    const rect = canvasMask.getBoundingClientRect()
    mouseMask.x = e.clientX - rect.left
    mouseMask.y = e.clientY - rect.top
});
document.querySelector('#canvas_mask').addEventListener('mouseup', (e) => {
    mouseMask.isDown = false
    const rect = canvasMask.getBoundingClientRect()
    mouseMask.x = e.clientX - rect.left
    mouseMask.y = e.clientY - rect.top
});

document.querySelector('#btn_generate_inpainting').addEventListener('click', async (e) => {
    const prompt = document.querySelector('#input_prompt').value
    const scale = parseFloat(document.querySelector('#input_scale').value)
    const strength = parseFloat(document.querySelector('#input_strength').value)
    const steps = parseInt(document.querySelector('#input_steps').value)
    toggleButtons()
    const image = await generateInpainting(
        document.querySelector('#canvas_selection').toDataURL('image/png', 1.0).replace("data:image/png;base64,", ''),
        document.querySelector('#canvas_mask').toDataURL('image/png', 1.0).replace("data:image/png;base64,", ''),
        prompt,
        {
            steps,
            strength,
            scale,
            width: inputWidth.value,
            height: inputHeight.value
        }
    )
    toggleButtons()
    lastGenWasInpaint = true

    const imageB64 = 'data:image/png;base64,' + image.toString('base64')

    const img = new Image();
    img.onload = function () {
        imageResult = img
        ctxResultComplete.drawImage(img, 0, 0, canvasResultComplete.width, canvasResultComplete.height)
        updateResultPanel()
    };
    img.src = imageB64
})

document.querySelector('#btn_generate_repainting').addEventListener('click', async (e) => {
    const prompt = document.querySelector('#input_prompt').value
    const scale = parseFloat(document.querySelector('#input_scale').value)
    const strength = parseFloat(document.querySelector('#input_strength').value)
    const steps = parseInt(document.querySelector('#input_steps').value)
    toggleButtons()

    console.log(JSON.stringify({
        scale, strength, steps, s: Math.ceil(steps / strength)
    }, null, 4))
    const image = await generateImageVariation(
        document.querySelector('#canvas_selection').toDataURL('image/png', 1.0).replace("data:image/png;base64,", ''),
        prompt,
        {
            steps,
            strength,
            scale,
            width: inputWidth.value,
            height: inputHeight.value
        }
    )
    toggleButtons()
    lastGenWasInpaint = false

    const imageB64 = 'data:image/png;base64,' + image.toString('base64')

    ctxResult.globalCompositeOperation = 'copy'
    ctxResult.fillStyle = 'rgba(0,0,0,1)'
    ctxResult.rect(0, 0, canvasSelection.width, canvasSelection.height)
    ctxResult.fill()


    const img = new Image();
    img.onload = function () {
        imageResult = img
        ctxResultComplete.drawImage(img, 0, 0, canvasResultComplete.width, canvasResultComplete.height)
        updateResultPanel()
    };
    img.src = imageB64
})

document.querySelector('#btn_generate_painting').addEventListener('click', async (e) => {
    const prompt = document.querySelector('#input_prompt').value
    const scale = parseFloat(document.querySelector('#input_scale').value)
    const strength = parseFloat(document.querySelector('#input_strength').value)
    const steps = parseInt(document.querySelector('#input_steps').value)
    toggleButtons()
    const image = await generateImage(
        prompt,
        {
            steps,
            strength,
            scale,
            width: inputWidth.value,
            height: inputHeight.value
        }
    )
    toggleButtons()
    lastGenWasInpaint = false

    const imageB64 = 'data:image/png;base64,' + image.toString('base64')

    const img = new Image();
    img.onload = function () {
        imageResult = img
        ctxResultComplete.drawImage(img, 0, 0, canvasResultComplete.width, canvasResultComplete.height)
        updateResultPanel()
    };
    img.src = imageB64
})

function updateResultPanel() {
    if (!imageResult) return

    if (lastGenWasInpaint) {
        ctxResult.globalCompositeOperation = 'copy'
        ctxResult.fillStyle = 'rgba(0,0,0,1)'
        ctxResult.rect(0, 0, canvasSelection.width, canvasSelection.height)
        ctxResult.fill()

        ctxResult.globalCompositeOperation = 'copy'
        ctxResult.filter = `blur(${document.querySelector('#input_blur_size').value}px)`
        ctxResult.drawImage(canvasMask, 0, 0, canvasSelection.width, canvasSelection.height)
        ctxResult.globalCompositeOperation = 'source-in'
        ctxResult.filter = 'blur(0px)'
        ctxResult.drawImage(imageResult, 0, 0)
    } else {
        ctxResult.globalCompositeOperation = 'copy'
        ctxResult.filter = 'blur(0px)'
        ctxResult.drawImage(imageResult, 0, 0)
    }
}

document.querySelector('#input_blur_size').addEventListener('input', async (e) => {
    updateResultPanel()
})


document.querySelector('#btn_paste_result').addEventListener('click', async (e) => {
    const newImage = document.querySelector('#canvas_result').toDataURL('image/png', 1.0)
    const image = new Image()
    image.onload = () => {
        const _width = inputWidth.valueAsNumber * inputZoom.valueAsNumber
        const _height = inputHeight.valueAsNumber * inputZoom.valueAsNumber

        ctxHidden.drawImage(image, 0, 0, canvasSelection.width, canvasSelection.height, mouse.x - _width / 2, mouse.y - _height / 2, _width, _height);
        const img = new Image();
        img.onload = function () {
            imageOnCanvas = img

            ctxResult.globalCompositeOperation = 'copy'
            ctxResult.fillStyle = 'rgba(0,0,0,0)'
            ctxResult.rect(0, 0, canvasSelection.width, canvasSelection.height)
            ctxResult.fill()
        }
        img.src = canvasHidden.toDataURL()
    }
    image.src = newImage;
})

document.querySelector('#btn_mask_mode_toggle').addEventListener('click', async (e) => {
    if (maskMode === 'Masking') maskMode = 'Unmasking'
    else maskMode = 'Masking'

    document.querySelector('#btn_mask_mode_toggle').innerHTML = `You are in ${maskMode} mode`
})

document.querySelector('#input_width').addEventListener('change', async (e) => {
    canvasSelection.width = inputWidth.value
    canvasResultSelection.width = inputWidth.value
    canvasResultComplete.width = inputWidth.value
    canvasMask.width = inputWidth.value
    canvasResult.width = inputWidth.value
    canvasResultSelection.style.left = inputWidth.value + 'px'
    canvasResult.style.left = inputWidth.value + 'px'
})

document.querySelector('#input_height').addEventListener('change', async (e) => {
    canvasSelection.height = inputHeight.value
    canvasResultSelection.height = inputHeight.value
    canvasResultComplete.height = inputHeight.value
    canvasMask.height = inputHeight.value
    canvasResult.height = inputHeight.value
})

document.querySelector('#btn_mask_toggle').addEventListener('click', async (e) => {
    canvasMask.hidden = !canvasMask.hidden
})


function renderScene() {
    requestAnimationFrame(renderScene);

    ctx.clearRect(0, 0, canvas.width, canvas.height)
    ctx.beginPath()

    if (imageOnCanvas) {
        ctx.drawImage(imageOnCanvas, 0, 0);
    }

    const _width = inputWidth.value * inputZoom.value
    const _height = inputHeight.value * inputZoom.value

    // selection viewport
    ctx.rect(mouse.x - _width / 2, mouse.y - _height / 2, _width, _height)
    ctx.strokeStyle = '#ffffff'
    ctx.lineWidth = 3
    ctx.stroke()
    ctx.rect(mouse.x - _width / 2, mouse.y - _height / 2, _width, _height)
    ctx.strokeStyle = '#000000'
    ctx.lineWidth = 1
    ctx.stroke()

    ctxSelection.beginPath()
    ctxSelection.clearRect(0, 0, canvasSelection.width, canvasSelection.height)
    ctxSelection.drawImage(canvasHidden, mouse.x - _width / 2, mouse.y - _height / 2, _width, _height, 0, 0, canvasSelection.width, canvasSelection.height)

    ctxResultSelection.beginPath()
    ctxResultSelection.clearRect(0, 0, canvasSelection.width, canvasSelection.height)
    ctxResultSelection.drawImage(canvasHidden, mouse.x - _width / 2, mouse.y - _height / 2, _width, _height, 0, 0, canvasSelection.width, canvasSelection.height)

    if (mouseMask.isDown) {
        if (maskMode !== 'Masking') {
            ctxMask.globalCompositeOperation = 'destination-out'
        }
        ctxMask.beginPath()
        ctxMask.fillStyle = 'rgb(255,255,255)'
        ctxMask.arc(mouseMask.x, mouseMask.y, document.getElementById('input_brush_size').value, 0, 2 * Math.PI)
        ctxMask.fill()
        if (maskMode !== 'Masking') {
            ctxMask.globalCompositeOperation = 'source-over'
        }
    }
}

requestAnimationFrame(renderScene);