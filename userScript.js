import ImageService from "./src/ImageService.js"
import {mkdirSync} from "fs";

const backendUrl = "http://localhost:5000/"
const savePath = "H:\\TEMP"

try {
    mkdirSync(savePath)
} catch {

}

const imgService = new ImageService(backendUrl, savePath, true)

function getRandomSeed() {
    return Math.floor(Math.random() * 1000000)
}

function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}

async function main() {
    // YOUR CODE HERE
    const prompt = `a beautiful! `

    const promptVariations = [

        '. attractive face, visible stomach. photography by ansel adams and peter kemp and Nina Leen and Edward Steichen. Cinematic, hyper realism, realistic proportions, ambient lighting, high detail 4k',
        '. attractive face, visible stomach. portrait by john william waterhouse and Edwin Longsden Long and Theodore Ralli and Nasreddine Dinet, oil on canvas. Cinematic, hyper realism, realistic proportions, dramatic lighting, high detail 4k',
        ', intricate, elegant, highly detailed, digital painting, artstation, concept art, smooth, sharp focus, illustration, art by artgerm and greg rutkowski and alphonse mucha and william - adolphe bouguereau',
        ', school swimsuit, finely detailed features, trending on pixiv, art by Artgerm and Greg Rutkowski and Alphonse Mucha ',
        ' as perfectly-centered-Portrait of a sun Goddess, intricate, highly detailed, digital painting, artstation, concept art, smooth, sharp focus, illustration, Unreal Engine 5, 8K, art by artgerm and greg rutkowski and alphonse mucha ',
        ' nude in blood shower, sexualized, ultra realistic, concept art, intricate details, eerie, horror, highly detailed, photorealistic, octane render, 8 k, unreal engine. art by artgerm and greg rutkowski and alphonse mucha ',
        ' in weta disney pixar movie still portrait photo : : as white gothic cowgirl woman by pixar : : by weta, greg rutkowski, wlop, ilya kuvshinov, rossdraws, artgerm, marvel, maxim cover, octane render, sweaty, torn, tattoos, bright morning, anime, liosh, mucha, artstation : : ',
        ' as a Warhammer 40k Battle Sister, portrait, fantasy, intricate, elegant, highly detailed, digital painting, artstation, concept art, smooth, sharp focus, illustration, art by artgerm and greg rutkowski and alphonse mucha ',
        ' as sorceress, dragon winged cloak, red hair, fantasy, fire!! full body, intricate, elegant, highly detailed, digital painting, artstation, concept art, smooth, sharp focus, illustration, art by artgerm and greg rutkowski and alphonse mucha ',
        ' as goddess of the forest, detailed face, clean lines, atmospheric lighting, amazing, full body, flowers, intricate, highly detailed, digital painting, artstation, concept art, sharp focus, illustration, art by greg rutkowski and alphonse mucha ',

    ]

    const subjects = ["scarlett johansson", "emma watson"]

    let i = 0
    while (i++ < 1000) {
        const seed = getRandomSeed()
        const lineBuffers = []
        for (let subject of subjects) {
            const imageBuffers = []
            const prompts = promptVariations.map(pv => `${prompt}${subject}${pv}`)
            for (let prompt of prompts) {
                imageBuffers.push(await imgService.generateImage(prompt, {
                    seed,
                    scale: 15,
                    steps: 200,
                    height: 704
                }))
                console.log("Image created!")
            }
            lineBuffers.push(await imgService.joinRight(imageBuffers, true))
            console.log("Line created!")
        }
        await imgService.joinDown(lineBuffers, true)
        console.log("Grid created! " + seed)
        console.log("Stop the app now if you want!")
        await sleep(3000)
        console.log("Too late...")
    }
}

main().then(function () {
    console.log("Done!")
})
