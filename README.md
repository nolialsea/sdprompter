# SD Prompter

Stable Diffusion Prompting tool, available in [Live version](https://nolialsea.gitlab.io/sdprompter/index2.html)  
To use with [this colab](https://colab.research.google.com/drive/1PQxlD_mb0uBL86WnvdYgtYOBg70vvKct)  
Colab exposes a REST API to use as Backend URL in SD Prompter  
The colab should work on free tier too  

# If you want to host the API yourself

First, install SD by downloading the official repo and copying the weights in the correct folder

Then, add the additional dependencies (you'll have to add them in your conda env if you are using conda):

```
pip install -qq -U diffusers transformers ftfy
pip install -qq "ipywidgets>=7,<8"
pip install python-dotenv
pip install flask
pip install flask_cors
pip install flask_cloudflared

pip install invisible-watermark
```

Create a `.env` file with your Huggingface access token:
```
HF_TOKEN=your_token_here
```

Copy the `rest_api.py` file of this repo in your SD `/scripts` folder and start it:
```
python scripts/rest_api.py
```

Don't forget to change the `map_location` in the file to use GPU instead of CPU if you have enough VRAM

# User script

First you need to have NodeJS installed in v16.x, and install the dependencies with `npm install`  
There is an example showing how to use JS to generate images automatically using simple logic.  
Check out the `userScript.js` file, edit it to fit your needs, then run this command:
```
node runUserScript.js
```

Of course you'll need to have NodeJS and install the dependencies with `npm install` first

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in your browser.

The page will reload when you make changes.\
You may also see any lint errors in the console.

### `npm test`

Launches the test runner in the interactive watch mode.\
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more
information.

### `npm run build`

Builds the app for production to the `build` folder.\
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.\
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

### `npm run eject`

**Note: this is a one-way operation. Once you `eject`, you can't go back!**

If you aren't satisfied with the build tool and configuration choices, you can `eject` at any time. This command will
remove the single build dependency from your project.

Instead, it will copy all the configuration files and the transitive dependencies (webpack, Babel, ESLint, etc) right
into your project so you have full control over them. All of the commands except `eject` will still work, but they will
point to the copied scripts so you can tweak them. At this point you're on your own.

You don't have to ever use `eject`. The curated feature set is suitable for small and middle deployments, and you
shouldn't feel obligated to use this feature. However we understand that this tool wouldn't be useful if you couldn't
customize it when you are ready for it.
